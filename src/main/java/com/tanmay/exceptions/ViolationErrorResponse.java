package com.tanmay.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ViolationErrorResponse {
	List<Violations> violation = new ArrayList<>();

	public List<Violations> getViolation() {
		return violation;
	}

	public void setViolation(List<Violations> violation) {
		this.violation = violation;
	}
	
	
}
