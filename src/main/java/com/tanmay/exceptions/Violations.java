package com.tanmay.exceptions;

public class Violations {
	private  String fieldName;
	private  String message;
	
	
	
	public Violations(String fieldName, String message) {
		super();
		this.fieldName = fieldName;
		this.message = message;
	}
	public String getFieldName() {
		return fieldName;
	}
	public String getMessage() {
		return message;
	}
	
	
}
