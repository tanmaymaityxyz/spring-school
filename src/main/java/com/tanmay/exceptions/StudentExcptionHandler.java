package com.tanmay.exceptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class StudentExcptionHandler {
	
//	@ExceptionHandler
//	public ResponseEntity<EntityException> exceptionForWrongParam(ConstraintViolationException e) {
//		EntityException le = new EntityException();
//		le.setStatus(HttpStatus.BAD_GATEWAY.value());
//		le.setMsg("Something went wrong");
//		
//		return new ResponseEntity<EntityException>(le,HttpStatus.BAD_REQUEST) ;
//	}
	
	
	@ExceptionHandler(ConstraintViolationException.class)
	  @ResponseStatus(HttpStatus.BAD_REQUEST)
	  @ResponseBody
	  List<String> onConstraintValidationException(
	      ConstraintViolationException e) {
		
		
		List<String> errorMesage = new ArrayList<>();
		Map<String,List<String>> errors = new HashMap<>();
		
		e.getConstraintViolations().stream().forEach(violation -> errorMesage.add(violation.getMessage()));
		
		return errorMesage;
	  }
	
	
}
