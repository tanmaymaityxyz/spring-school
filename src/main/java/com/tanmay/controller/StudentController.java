package com.tanmay.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tanmay.entity.Student;
import com.tanmay.service.StudentService;

@RestController
@Validated
public class StudentController {
	
	
	@Autowired
	private StudentService studentService;
	
	
	@PostMapping("/add/student")
	public void studentRegister(@RequestParam("kname") String name,
			@RequestParam("dob") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dob,
			@RequestParam("parentName") String parentName,
			@RequestParam("Class") String classInfo) {
		
		Student s1 = new Student(name,parentName,dob,classInfo);
		
		studentService.addStudent(s1);
		
	}
	
	@PostMapping("/student/add/role")
	public void addNewRole(@RequestParam("rollno")int rollno,@RequestParam("role") String theRole) {
		studentService.addRole(rollno, theRole);
	}
	
	@GetMapping("/show/all")
	public List<Student> showAllStudent() {
		return studentService.getAllStudent();
	}
}
