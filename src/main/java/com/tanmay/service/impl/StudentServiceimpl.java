package com.tanmay.service.impl;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.tanmay.entity.Role;
import com.tanmay.entity.Student;
import com.tanmay.repository.RoleRepository;
import com.tanmay.repository.StudentRepository;
import com.tanmay.service.StudentService;


@Service
@Validated
public class StudentServiceimpl implements StudentService {
	
	@Autowired
	private StudentRepository stdRepo;
	
	@Autowired
	private RoleRepository roleRepo;
	@Transactional
	@Override
	public void addStudent(@Valid Student std) {
		
		std.getRoles().add(new Role(1,"ROLE_STUDENT"));
		stdRepo.save(std);
	}

	@Override
	public void addRole(int roll, String role) {
		Student student = stdRepo.findById(roll).orElseThrow(() ->  new RuntimeException("No student Found with given roll"));
		student.getRoles().add(roleRepo.findByName(role).orElseThrow(() -> new RuntimeException("No role found of given name")));
		stdRepo.save(student);
	}

	@Override
	public List<Student> getAllStudent() {
		return stdRepo.findAll();
	}

}
