package com.tanmay.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.tanmay.entity.Student;

@Service
@Validated
public interface StudentService {
	public void addStudent(@Valid Student std);
	
	public void addRole(int roll,String role);

	public List<Student> getAllStudent();
}
