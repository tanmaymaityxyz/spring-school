package com.tanmay.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;


@Entity
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int StudentRole;
	
	@NotBlank(message = "Your name must not be blank")
	private String name;
	
	@NotBlank(message = "Your parents name must not be blank")
	private String parentsName;
	
	@NotNull(message = "You must provide your birthday")
	@Past(message = "Your date is invalid as it is after today")
	private LocalDate dob;
	private String classInfo;
	
	
	@ManyToMany(cascade = {CascadeType.PERSIST})
	@JoinTable(joinColumns = {@JoinColumn(name = "std_id")},inverseJoinColumns ={@JoinColumn(name="role_id")},name = "student_role")
	List<Role> roles = new ArrayList<>();
	
	public Student() {
		
	}
	
	public Student(@NotBlank String name, @NotBlank String parentsName, @NotNull @Past LocalDate dob,
			String classInfo) {
		super();
		this.name = name;
		this.parentsName = parentsName;
		this.dob = dob;
		this.classInfo = classInfo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentsName() {
		return parentsName;
	}
	public void setParentsName(String parentsName) {
		this.parentsName = parentsName;
	}
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public String getClassInfo() {
		return classInfo;
	}
	public void setClassInfo(String classInfo) {
		this.classInfo = classInfo;
	}
	
	

	public int getStudentRole() {
		return StudentRole;
	}

	public void setStudentRole(int studentRole) {
		StudentRole = studentRole;
	}
	
	

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", parentsName=" + parentsName + ", dob=" + dob + ", classInfo=" + classInfo
				+ "]";
	}
	
	
	
}
